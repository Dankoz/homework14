
#include <iostream>
#include <string>

int main()
{
    std::string welcome;
    
    std::cout << "Enter some text" << std::endl;
    std::getline(std::cin, welcome);

    std::cout << welcome << std::endl;
    std::cout << "String length:" << welcome.length() << std::endl;
    std::cout << "String starts with: " << welcome[0] << std::endl;
    std::cout << "String ends with: " << welcome[welcome.length() - 1] << std::endl;
}

